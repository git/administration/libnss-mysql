
MariaDB/MySQL for NSS systems
=============================

libnss-mysql is a Network Security Services library module interfacing
user account information with a MariaDB/MySQL database.  This allows
keeping user account data in an SQL database authenticating user
accounts on a system.

This project is a continuation of the work of Ben Goodwin
<cinergi@users.sourceforge.net>.  The last commit in the version
history from Ben was 2006.  It is now 2024 some 18 years later.  That
this code held up so well for so many years is a testament to its
quality and maturity.  The motivation for code maintenance now is that
MariaDB has changed interface specification and modifications are
needed in order to keep this code working on current systems.

Building from Source
====================

Building from a git source clone acts as a developer requires all
three of the autotools to be installed as well as the C compiler and
essential build environment.

    git clone git://git.savannah.gnu.org/administration/libnss-mysql.git
    cd libnss-mysql
    ./bootstrap
    ./configure
    make

Building from a distribution tar bundle does not require any of the
developer autotools and only requires the C compiler.  This may be
more suitable for non-developers.

    tar xzf libnss-mysql-X.Y.Z.tar.gz
    cd libnss-mysql
    ./configure
    make

Configuration and Use
=====================

    Sample configurations for these files are available.  Install a
    suitable version for your system environment.

        + /etc/libnss-mysql.cfg
	+ /etc/libnss-mysql-root.cfg

    The /etc/nsswitch.conf is configured to use this library by adding
    "mysql" to the query sources fields.

        passwd: files mysql
        shadow: files mysql
        group:  files mysql

GNU Savannah Community
======================

You can communicate with me Bob Proulx <bob@proulx.com> and the GNU
Savannah Hackers who are maintaining this version of the sofware by
emailing the <savannah-hackers-public@gnu.org> public mailing list.

Program License
===============

Copyright 2024 Bob Proulx <bob@proulx.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see <https://www.gnu.org/licenses/>.

File License
============

Copyright 2024 Bob Proulx <bob@proulx.com>

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.
